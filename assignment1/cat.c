/*
* Author: Daniel Devine
* Date: 27/02/2013
* Description: A simple clone of `cat`. Mirrors GNU cat except for line numbering with multiple files.
*/

#include "cat.h"

int main(int argc, char **argv){
    // If given arguments, enter related if statement.
    if (argc > 1){
        // Process the arguments
        int option_index = 0;
        // Note: If a colon follows an option in the opt string it requires an argument.
        char opt;

        while ((opt = getopt_long(argc, argv, optstr, long_options, &option_index)) != -1){
            switch (opt){
                case 'E':
                    {
                        markendlines_flag = 1;
                        break;
                    }
                case 'e':
                    {
                        markendlines_flag = 1;
                        shownonprinting_flag = 1;
                        break;
                    }
                case 'n':
                    {
                        numberlines_flag = 1;
                        break;
                    }
                case 'v':
                    {
                        shownonprinting_flag = 1;
                        break;
                    }
                case 's':
                    {
                        squeezeblanks_flag = 1;
                        break;
                    }
                case 'b':
                    {
                        numbernonblanks_flag = 1;
                        break;
                    }
                case 't':
                    {
                        shownonprinting_flag = 1;
                        showtabs_flag = 1;
                        break;
                    }
                case 'T':
                    {
                        showtabs_flag = 1;
                        break;
                    }
                default:
                    fprintf(stderr, "Unknown character/option '%c'\n", opt);
                    return EXIT_FAILURE;
            }
        }

        // Check flags, add to pipeline. This is to enforce an order of operations in the pipeline.
        if (numbernonblanks_flag){
            pipeline_stack[pipeline_size++] = &numbernonblanks;
        }

        pipeline_stack[pipeline_size++] = &linewatcher; // Always have a line watcher in the pipeline.

        if (numberlines_flag && !numbernonblanks_flag){
            pipeline_stack[pipeline_size++] = &numberlines;
        }
        if (markendlines_flag){
            pipeline_stack[pipeline_size++] = &markendlines;
        }
        if (squeezeblanks_flag){
            pipeline_stack[pipeline_size++] = &squeezeblanks;
        }
 
        // Start shoving files into the pipeline.

        int o = optind; // optind is weird.
        if (o < argc && o > 0){
            // Remaining arguments are files.
            while(o < argc){
               FILE *fp;
               fp = fopen(argv[o++], "r");
               if (fp != NULL){
                   filesprovided = 1;
                   enter_pipeline(fp, stdout);
               } else {
                  perror("main() - file arguments");
                  return EXIT_FAILURE;
               }

               // Check for error in switcher.
               fclose(fp);
            }
        } else {
            // Just use stdin, rather than iterating over files.
            enter_pipeline(stdin, stdout);
        }
    } else {
        // Just copy stdin to stdout, using the fastest method.
        int stream = fstream(stdin, stdout);
        if (stream < 0){
            perror("main() - file streaming");
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

void enter_pipeline(FILE *in, FILE *out){
    // Pushes data into the pipeline functions.
    currchar = getc(in);
    lastchar = '\0';
    iterator = 1;
    firstiter = 1;

    while(feof(in) == 0){
       nextchar = getc(in);

       *after_buf = "";
       *before_buf = "";
       omit_currchar = 0;
       hide_currchar = 0;
       before_buf_pos = (char*) before_buf;
       after_buf_pos = (char*) after_buf;

       for (int i = 0; i < pipeline_size; i++){
           pipeline_stack[i]();
       }

       if (omit_currchar == 0){ // If the current character is to be omitted, don't put anything.
          if (before_buf_pos != (char*) before_buf){ // If there is something in before_buf, put it.
              fputs((char*) before_buf, out);
          }

          // Put the current character, or use a replacer.
          if (!hide_currchar){
              if (!shownonprinting_flag && !showtabs_flag){
                  putc(currchar, out);
              } else if(shownonprinting_flag && showtabs_flag){
                  if(currchar != '\t'){
                      convertnonprint(currchar, out);
                  } else {
                      showtabs(currchar, out);
                  }
              } else if(shownonprinting_flag) {
                  convertnonprint(currchar, out);
              } else {
                  showtabs(currchar, out);
              }
          }

          if(after_buf_pos != (char*) after_buf){ // If there is something in after_buf, put it. 
             fputs((char*) after_buf, out);
          }
       }

       if(addnewlineafter == 1){
           putc('\n', out);
           addnewlineafter = 0;
       }

       if (ferror(out) != 0){
           perror("enter_pipeline() output");
       }

       // Set up for next iteration
       lastchar = currchar;
       currchar = nextchar;
    }
    return; 
}

int fstream(FILE *in, FILE *out){
    // Stream the in file to the out file. Return negative on error.
    while (feof(in) == 0){
        putc(getc(in), out);
        if (ferror(in) != 0){ // ferror checks if there was an error in the stream.
            perror("fstream()");
            return -1;
        }
    }
    return 1;
}

void linewatcher(){
    // Keeps track of line numbers. This keeps line numbers consistent across pipeline functions.
    // The squeezeblanks function also relies on this function to help control output.
    if (lastchar == '\n' && squeeze < 2){
        if (!freezeiter){
            iterator++;
        }

        if (firstiter == 2){
            firstiter = 0;
        }
    }

    if (squeeze >= 2){
        omit_currchar = 1;
        if (nextchar != '\n'){
            squeeze = 0;
        }
    }
    return;
}

void numberlines(){
    // Prepend a line number before each line.
    if (firstiter == 1 || lastchar == '\n'){
        firstiter = 2;
        sprintf(before_buf_pos, "%6li\t", iterator);
        before_buf_pos += 8;

        if (nextchar == EOF){
            addnewlineafter = 1;
        }
    }
    return;
}

void numbernonblanks(){
    // Prepend a line number before each line, if the line is not blank.
    if ((lastchar == '\n' || lastchar == '\0') && currchar != '\n'){
        numberlines();
        freezeiter = 0;
    } else {
        freezeiter = 1;
    }
    return;
}

void markendlines(){
    // Append a '$' to each line.
    if (currchar == '\n' && filesprovided){
        sprintf(before_buf_pos++, "$");
        return;
    }

    if (currchar == '\n' && !filesprovided){
        hide_currchar = 1;
    }

    if (nextchar == '\n' && !filesprovided){ // This is for typing into stdin... `cat -E`
        sprintf(after_buf_pos++, "$");
        addnewlineafter = 1;
    }
    return;
}

void caretcontrolcode(char in, FILE *out){
    // Convert all non-printing characters in the C0 set to strings before putting them into out.
    if (in >= 0x00 && in <= 0x1f){ // C0 control codes go from 0x00 to 0x1f
       putc('^', out);
       putc('@' + in, out);
    } 
    else if(in == 0x7f){
        fputs("^?", out);
    } else {
        // Anything we don't recognise, just put through.
        putc(in, out);
    }

    if (ferror(out) != 0){
        perror("caretcontrolcode()");
    }
    return;
}

void convertnonprint(char in, FILE *out){
    // Convert the non-printable control codes and put them in out.
    switch(in){
        // These explicitly stated characters are to be certainly put through as the default case will block them.
        // Everything in the block except LFD and TAB - the code has been done this way to make configuration of
        // the behavior of characters in this block flexible. Although some characters are printable it may not be
        // desirable output (\f).
        case '\a':
        case '\b':
        case '\v':
        case '\f':
        case '\r':
        case 0x7f: // DEL, ^? - Not technically part of C0
           caretcontrolcode(in, out);
           break;
        default:
        {
            if ((in <= 0x1f && in >= 0x00) && !(in > 0x06 && in < 0x0f)){ // If it is a control code and not one of the printable ones.
                caretcontrolcode(in, out); 
            } else {
                putc(in, out);
                if (ferror(out) != 0){
                    perror("convertnonprint()");
                }
            }
        }
    }
    return; 
}

void squeezeblanks(){
    // Compress multiple blank lines into a single blank line. 
    // If lines are being numbered, they will appear to increment normally (no skipping).
    if (currchar == '\n' && nextchar == '\n'){ 
        squeeze++;
        if (iterator == 1){ // If the first line is a blank, and then we keep one blank after that, we will have too many blanks.
            squeeze = 2; // Skip straight to cutting out blank lines, as we already have our first blank that the rest are squeezed into.
        }
        squeeze = squeeze > 2 ? 2 : squeeze; // This way we will never go over the sign bit.
    } else {
        squeeze = 0;
    }
    return;
}

void showtabs(char in, FILE *out){
    //  Show tabs as ^I
    if (currchar == '\t'){
       fputs("^I", out);

       if (ferror(out) != 0){
           perror("showtabs()");
       }
    }
    return;
}
