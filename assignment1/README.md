This file is in [Markdown format](http://daringfireball.net/projects/markdown/).

MILESTONES 1, 2, 3
==================

In milestones 1, 2 and 3 a BSD flavoured `cat` program was produced (so far only tested in GNU userland, but apparently works under Apple OSX). The code is available in my [Systems Programming repository](https://bitbucket.org/ddevine/systems-programming) under the MIT license.

There is also a Hello World program.

MILESTONE 4
============

In milestone 4 I am producing a simple CLI. I've called it `sh` though I doubt it will actually ever fully implement real `sh` functionality.

The first iteration (at e8e4fdf) of `sh` program uses the stdlib `system()` function to execute a command line retrieved from stdin by the stdio `getline()` function. The shell exits on an input error (when `getline()` returns -1), CTRL+C/SIGINT or when `exit` is entered.

The second iteration of `sh` will use unistd `execvp()` in place of `system()`. `execvp()` replaces the current process image with another. When you replace a process image with another you are essentially replacing the execution of a program with another.

The difference between `execvp()` and `system()` is that in `system()` the process of searching the `PATH` for the executable and then using `fork()` and `wait()` to spawn a new process and return from the process execution is handled automatically. `execvp()` also differs from `system()` in that it expects a `FILE` pointer to be given along with an array of pointers to the argument strings (NULL pointer terminated).

MILESTONE 5
============

In milestone 5 I am extending the simple CLI into a more complete shell.

The shell includes a `cd` command which is implemented via the `chdir()` system call. The reason this is implemented as a call to `chdir()` and not simply a call to system's `cd` utility is that we want all the resulting commands given after `cd` to be in the context of the new current working directory. A simple call to the system's `cd` utility would happen as a child process and the directory change would not persist for the next child as the parent would not inhert the new current working directory from the child. By implementing `cd` internally in the shell using `chdir()`, the child processes inherit the current working directory from the parent, due to the semantics `fork()`, used to create the child process.

MILESTONE 6
============

For milestone 6 `readline()` and pipes are going to be added.

`readline()` is a GNU library which adds features such as editor type functionality, key bindings, auto-completion and history to a command line prompt. Readline is a very common library and greatly simplifies adding modern CLI type functionality to a program, because it means that you don't have to write a loop to listen for and interpret terminal control codes (which may not be cross-system compatible). `readline()` can take a `prompt` string argument, which has the advantage of making sure that the terminal prompt cannot be accidentially edited or removed by a user.

In POSIX, unnamed pipes (those which are not expressed as filesystem node) are created via the `pipe()` system call and their purpose is to allow inter-process communication. After a `fork()` call the child will inherit the file descriptors set up by `pipe()` which allow it to communicate with the parent process. Pipes are requested in a shell language usually by the '|' character, which is usually called the "pipe character". Pipes are integral to realisation of the Unix philosophy of creating powerful and complex functions from combined smaller, simpler programs.
