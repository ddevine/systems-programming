/*
* Author: Daniel Devine
* Date: 27/03/2013 
* Description: Header file for sh.c, a simple shell.
*/

#ifndef SH_H
#define SH_H
    // The _GNU_SOURCE macro must go before stdlib
    #define _GNU_SOURCE

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <ctype.h>
    #include <readline/readline.h>
    #include <readline/history.h>

    // Enumerated type to define the states that can occur while lexing a line.
    typedef enum state {
         STATE_GENERAL, // In general state, do nothing or initialise entry into next state.
         STATE_IN_WORD, // On word state, store char in word or terminate word (aka "token"). 
                        // Note that unescaped special chars make their own token.
         STATE_IN_ESCAPE, // On escape state, set escape flag. If escape flag next char is taken literally. A new word may be started.
    } state;

    // Enumerated type to define the categories of characters
    typedef enum category {
        CHAR_GENERAL = 'A',
        CHAR_WHITESPACE = ' ',
        CHAR_END = EOF, // This will cause the state machine to stop. Integer value -1 usually.
        CHAR_SPECIAL = '|',
        CHAR_ESCAPE = '\\',
        CHAR_UNKNOWN = -2, // Not sure if/when this should arise.
    } category;

    // Prototypes
    void cleanup();
    void sh(char *cmdline);
    int exec_statement(int pos, int *statements, int *pipe_statements, int len);
    int get_statement_length(int pos);
    int subprocess(int pos, int background);
    void addtostring(char c, int *buffpos, int inc);
    void pushtoken(int *length);
    void setup_redirs(int pos, int length);
    void source(char *filestr);
    int shlex(char *line);
    category catofc(char c);
    int cd();
    void child_handler(int sig);

    // Points to the command line string given by a user.
    char *lineptr = NULL;

    // The arguments array
    char **args;

    // Number of args.
    int numargs;

    // The buffer.
    char *buffer;

    // Keep track of process numbers by positionally putting pids. We will just allocate 100 possible, and put ? when exceeded.
    pid_t pidarr[100];

    // Keep track of how many processes currently running.
    int processes = 0;

    // The size of the buffer given, ignored.
    size_t buffsize = 0;

    // The input file for the CLI
    FILE *in = NULL;

    // The output file for the CLI
    FILE *out = NULL;

    // A flag to signal if the terminal should wait for the child or not.
    int waitforchild = 1;

    // Global file descriptors, so that if a file descriptor needs to be closed after background process, it can be done in the handler.
    int fds[2];
#endif
