#ifndef CAT_H_INCLUDED
#define CAT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h> // I want to support long options. Short options only in unistd.h version.
#include <string.h>

// Functions
int fstream(FILE *in, FILE *out);
void enter_pipeline(FILE *in, FILE *out);

// Pipeline methods.
void numberlines();
void markendlines();
void linewatcher();
void squeezeblanks();
void numbernonblanks();

// Replacer functions.
void showtabs(char in, FILE *out);
void convertnonprint(char in, FILE *out); // This is for the C/Cat specific subset of C0 control codes.
void caretcontrolcode(char in, FILE *out); // This is for all C0 control codes

// Struct describing options for getopt.
static struct option long_options[] = {
    {"number", no_argument, 0, 'n'},
    {"show-ends", no_argument, 0, 'E'},
    {"show-nonprinting", no_argument, 0, 'v'},
    {"squeeze-blank", no_argument, 0, 's'},
    {"number-nonblank", no_argument, 0, 'b'},
    { NULL, no_argument, 0, 'e'},
    {"show-tabs", no_argument, 0, 'T'},
    { NULL, no_argument, 0, 't'},
    {0, 0, 0, 0}
};

// The options string for getopt.
char optstr[] = "nEvsbetT";

// Argument flags.
int numberlines_flag = 0;
int markendlines_flag = 0;
int shownonprinting_flag = 0;
int squeezeblanks_flag = 0;
int numbernonblanks_flag = 0;
int showtabs_flag = 0;

// This is the size of the pipeline.
#define MAX_PIPELINE_SIZE 5

// Output buffer
#define BUFFER_SIZE 25

// The pipeline will be an array of function pointers.
void (*pipeline_stack[MAX_PIPELINE_SIZE])(); 

// The current size of the pipeline.
int pipeline_size = 0;

// Iterator keeps track of the number of lines.
unsigned long iterator = 1; // This will be used even when using cat to operate on a binary file, so it needs to be huge.

// Freeze iteration
int freezeiter = 0;

// Keeps track if this is the first line of an input.
int firstiter = 1;

// Add newline flag
int addnewlineafter = 0;

// Buffer for things occuring before currchar
char *before_buf[BUFFER_SIZE];

// Index of last item in before_buf
char *before_buf_pos = NULL;

// Omit the current character, and also stop printing of buffers.
int omit_currchar = 0;

// Hide the current character, but print the buffers.
int hide_currchar = 0;

// Buffer for things occuring after currchar
char *after_buf[BUFFER_SIZE];

// Index of last item in after_buf
char *after_buf_pos = NULL;

// Last character
unsigned char lastchar;

// Current character
unsigned char currchar;

// Next character
unsigned char nextchar;

// Keep track of if line squeezing is active.
int squeeze = 0;

// Flag for if files are provided (other than stdin).
int filesprovided = 0;

#endif
