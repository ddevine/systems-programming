/*
* Author: Daniel Devine
* Date: 27/03/2013
* Description: A simple shell.
*/
#include "sh.h"

int main(int argc, char **argv){
    in = stdin;
    out = stdout;
    int cont = 1; // Keeps track if whether the input loop should continue.

    char cwdbuff[256];
    char prompt[384];

    fputs("\e[1;1H\e[2J", out); //ANSI escape sequence, clear screen.

    while(cont == 1){
        getcwd(cwdbuff, (size_t) 255 * sizeof(char)); 
        sprintf(prompt, "[%s]$ ", basename(cwdbuff));
        lineptr = readline(prompt); // If you give readline a prompt string it ensures it can't be deleted by user.
        if (lineptr == NULL){ // Either failure or EOF.
            cont = 0;
        } else { 
            if (strcmp(lineptr, "exit") != 0){ // We should extra whitespace from lineptr.
                sh(lineptr);
                add_history(lineptr);
            } else {
                cont = 0;
            }

            if (lineptr != NULL){
                free(lineptr);
            }
        }
    }

    return EXIT_SUCCESS;
}

void sh(char *line){

    numargs = shlex(line);

    if (numargs == 0){
        return;
    }

    int statement_index = 0;
    int statements[numargs]; // Stores indexes to the beginning of each statement in args.
    int pipe_statements[numargs]; // Marks if the statement at the index is a pipe statement.


    // Clean up array content
    for (int i = 0; i < numargs; i++){
        statements[i] = 0;
        pipe_statements[i] = -1;
    }

    // POPULATE THE ARRAYS. 
    
    // For each token in args
    for (int i = 0; i < numargs; i++){
        // if token is a terminator token (\n ; |)
        if ((args[i] != NULL) && (strcmp(args[i], "|") == 0 || strcmp(args[i], ";") == 0)){
            #ifdef DEBUG
            printf("Found terminating token: %s\n", args[i]);
            #endif

            // if the terminator was a pipe, add this statement to pipe_statements.
            if (strcmp(args[i], "|") == 0 && args[i + 1] != NULL){
                pipe_statements[statement_index++] = 1; // Mark the statement as a pipe statement.
                #ifdef DEBUG
                printf("Statement %i is a pipe statement, due to '|' at token index: %i\n", statement_index, i);
                #endif
            }

            // Add this statement to statements array.
            if(args[i + 1] != NULL){
                statements[statement_index] = i + 1; // The statement starts after a terminator token.
                #ifdef DEBUG
                printf("Statement beginning at token %i added at statements index: %i\n", i, statement_index);
                #endif
            }

            args[i] = NULL; // Terminate statement.
            statement_index++;
        }

        if (i == 0 && args[0] != NULL){
            // Add first statement to statements.
            statements[0] = 0;
            #ifdef DEBUG
            printf("0th statement added.\n");
            #endif
        }
    }

    #ifdef DEBUG
    printf("statements: %i\n", statement_index + 1);
    #endif


    // PROCESS THE STATEMENTS

    // for each statement in statements
    for (int i = 0; i < (statement_index + 1); i++){
        // if first statement or previous not a pipe statement (because if it is, this has been run by exec_statement)
        if (i == 0 || pipe_statements[statements[i]] != 1){
            #ifdef DEBUG
            printf("Calling exec_statement on args index: %i\n", statements[i]);
            #endif
            exec_statement(statements[i], statements, pipe_statements, statement_index + 1);
        }
    }
    
    // Clean up the args array as it is now unneeded.
    for (int i = 0; i < numargs; i++){
        free(*(args + i++));
    }

    return;
}


int exec_statement(int pos, int *statements, int *pipe_statements, int length){
    // Execute a statement and child statements if there are any (due to pipes). Can be used recursively. Relies on the `args` global.
    // Returns status (needed for "&&").

    if (args[pos] == NULL){
        return EXIT_SUCCESS; // Well... nothing failed.
    }

    int background = 0; // A flag to indicate whether the statement should be run as a background process.
 
    pid_t chpid; // The pid for the first fork, for traditional subprocess execution - or for splitting into pipe subprocesses.

    int statement_length = get_statement_length(pos);

    // if the last token is a '&' set background flag
    if(strcmp(args[pos + statement_length -1], "&") == 0){
        background = 1;
        args[pos + statement_length -1] = NULL;
    }

    // if the first token is "cd" go do cd()
    if (strcmp(args[pos], "cd") == 0 && args[pos + 1] != NULL){
        return cd(args[statements[pos] + 1]); // We should just be able to return here, I think.
    }

    // if we have a source command, start a new process with stdin redirected from input file
    // wait until that process returns and continue here. We can't recursively call because of our global args array!
    // This source command does not behave like a real source command.
    if (strcmp(args[pos], "source") == 0 && args[pos + 1] != NULL){
        source(args[pos]);
        pos += 1;
        return EXIT_SUCCESS;
    } else if (strcmp(args[pos], "source") == 0 && args[pos + 1] == NULL){
        fprintf(stderr,"No source file argument for source command given.\n");
        return EXIT_FAILURE;
    }


    // if pipe_statement
    if (pipe_statements[pos] == 1){
        pipe(fds); // This sets up the pipe for IPC

        chpid = fork();


        if (chpid == -1){ // Fork may fail
            perror("Fork failed");
            return EXIT_FAILURE; 
        }

        // if child
        if (chpid == 0){
            // close unused write side
            close(fds[1]);

            // Copy descriptor back...
            dup2(fds[0], STDIN_FILENO);

            // pos += 1
            pos += 1; // (what should we do if this is NULL? Automatically set stdout? Error?)
            statement_length = get_statement_length(statements[pos]);

            if (args[pos] == NULL){
                return EXIT_FAILURE; // We dun goofed.
            }

            // if pipe_statement
            if (pipe_statements[pos] == 1){
                // recurse exec_statement
                exec_statement(statements[pos + 1], statements, pipe_statements, length);
            } else {
                // return subprocess
                setup_redirs(pos, statement_length);
                int ret = subprocess(statements[pos], background);
                close(fds[0]);
                return ret;
            }
        } else {
            // In the parent

            // close unused read side
            close(fds[0]);

            // Copy piped descriptor into stdout.
            dup2(fds[1], STDOUT_FILENO);

            // return subprocess
            setup_redirs(pos, statement_length);
            int ret = subprocess(statements[pos], background);
            if (!background){
                close(fds[1]); // Close write side now that we are done. Passes EOF through pipe.
            }
            return ret;
        }
    } else {
        // return subprocess
        setup_redirs(pos, statement_length);
        return subprocess(statements[pos], background);
    }
    return EXIT_FAILURE;
}

void source(char *path){
    // Read commands from a file
    // We need to start a process with stdin redirected to the file.
    pid_t chpid = 0;
    int chstatus = 0;

    // fork
    chpid = fork();

    // if child 
    if(chpid == 0){

        FILE *fpi = freopen(path, "rb", stdin);

        if (fpi == NULL){
            fprintf(stderr, "Invalid/inaccessible file given for input redirection.");
            return;
        }

        char *line = NULL;

        // while readline
        while(readline(line) != NULL){
            sh(line);
            free(line);
        }
        return;
    } else {
    // if parent
        // wait
        while (wait(&chstatus) != chpid);
        // Return
        return;
    }
}

int get_statement_length(pos){
    // Counts the number of tokens in a statement.
    int length = 0;
    while(args[pos + length] != NULL){
        length++;
    }

    #ifdef DEBUG
    printf("statement length at pos %i is %i\n", pos, length);
    #endif

    return length;
}

int subprocess(int pos, int background){
    pid_t chpid;
    int chstatus = EXIT_FAILURE;

    if (background){ //Originally !background???
        if (signal(SIGCHLD, child_handler) == SIG_ERR){
            fprintf(stderr, "Failed to attach signal handler.\n");
            return EXIT_FAILURE; // Because we don't want zombies, and there is probably something else broken.
        }
    } else {
        signal(SIGCHLD, SIG_DFL);
    }
   
    // fork
    chpid = fork();

    if (chpid == -1){ // Fork may fail
        perror("fork failed");
        return EXIT_FAILURE; 
    }

    if (chpid == 0){
        int tlen = strlen(args[pos]);
        char nonbasename[tlen+1];
        strcpy(nonbasename, args[pos]);
        args[pos] = basename(args[pos]);            

        execvp(nonbasename, args + pos); // The first argument should be the full path, but arg0 should be just the executable name.

        // If execvp returns then it actually failed.
        perror("");
        if(kill(getpid(), SIGTERM) != 0){
            perror("kill failed");
        }
        return EXIT_FAILURE;

    } else {
        if (!background){
            while (wait(&chstatus) != chpid);
            return chstatus;
        } else {
            if (processes < 100){
                pidarr[processes++] = chpid;
            }
            fprintf(stdout, "[%i] %i\n", processes, (int) chpid);
        }

        return EXIT_SUCCESS; // Uh, I guess. This may be quite wrong though. Would this screw with && semantics?
    }

}


void setup_redirs(int pos, int length){
    // Set up I/O redirection given a statement to process. Relies on args global.

    int inset = 0;
    int outset = 0;
    for (int i = 0; i < length; i++){
        if(args[pos + i] != NULL){
            if (strcmp(args[i], "<") == 0 && !inset){
                // stdin redir from a file
                inset = 1;
                if ((i + 1) < length && (args[pos + (i+1)] != NULL)){
                    FILE *fpi = freopen(args[pos + (i+1)], "rb", stdin);

                    if (fpi == NULL){
                        fprintf(stderr, "Invalid/inaccessible file given for input redirection.");
                    }

                    args[pos + i] = NULL; // Replace '<', end args. We only support one redirection per statement.
                    #ifdef DEBUG
                    printf("Redirecting stdin.\n");
                    #endif
                } else {
                    // Error, we don't have a file!
                    fprintf(stderr, "No file for input redirection specified.");
                }
            } else if(strcmp(args[pos + i], ">") == 0 && !outset){
               // stdout redirect to a file
               outset = 1;
               if ((i + 1) < length && (args[pos + (i+1)] != NULL)){
                    FILE *fpo = freopen(args[pos + (i+1)], "w+", stdout);

                    if (fpo == NULL){
                        fprintf(stderr, "Invalid/inaccessible file given for input redirection.");
                    }

                    args[pos + i] = NULL; // Replace '>', end args. We only suport one redirection per statement.

                    #ifdef DEBUG
                    printf("Redirecting stdout.\n");
                    #endif
                } else {
                    // Error, no file...
                    fprintf(stderr, "No file for output redirection specified.");
                }
            }
        }
    }
    return;
}


category catofc(char c){
    // Returns the category relating to the character `c`. Category is defined in the `category` type.

    //printf("catofc(c) : %c\n", c);
    if (isalnum(c) != 0) // Include underscore as a "general character".
         return CHAR_GENERAL;
 
    switch (c){
        case '_': return CHAR_GENERAL;
        case ' ': case '\t': return CHAR_WHITESPACE;
        case '<': case '>': case '|': case '&': case '"': case ';': return CHAR_SPECIAL;
        case EOF: case '\0': return CHAR_END;
        case '\\': return CHAR_ESCAPE;
        default:{
                if (ispunct(c) != 0) return CHAR_GENERAL; // Let the rest of the un-special punctuation through.

                fprintf(stderr, "catofc(c) fell through to CHAR_UNKNOWN: '%c'\n", c);
                return CHAR_UNKNOWN;
            }
    }
    return CHAR_UNKNOWN;
}

int shlex(char *line){
    // Shell lexer. Tokensises a string of shell language input.
    // Tokenise the string `line` and store tokens in the `args` string array, return number of tokens.

    if (*(line + strlen(line) - 1) == '\n') // Strip newline from end if there is one.
       *(line + strlen(line) - 1) = '\0';

    #ifdef DEBUG
    printf("Line is: \"%s\"\n", line);
    #endif    

    category cat;
    state current_state = STATE_GENERAL;
    state next_state = STATE_GENERAL;
    char c = *(line++);
    int buffinc = 20; // The initial size of the buffer, and the increment to realloc by.
    int buffpos = 0;
    int selector = 0;
    int nargs = 0;
    int flag_literal = 0;

    args = malloc(sizeof(char*));
    if(args == NULL){
        fprintf(stderr, "malloc failed for args.\n");
        abort();
    } else {
        *(args) = NULL;
    }

    buffer = malloc(buffinc + sizeof(char));
    if (buffer == NULL){
        fprintf(stderr, "malloc failed for buffer.\n");
        abort();
    }

    do {
        // Cycle through characters, create tokens.  

        // Get character category
        cat = catofc(c);

        // Make a uniqified selector.
        selector = (1000 * current_state) + cat;

        // The cases in this switch are based off a state transition table.
        switch(selector){
       // STATE: GENERAL
            case (1000 * STATE_GENERAL + CHAR_GENERAL):{
                // Kick off the token buffer, add the first char. Note this state is similar to the IN_WORD version.
                #ifdef DEBUG
                printf("state state_general char_general %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc); 
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_GENERAL + CHAR_ESCAPE):{
                // Enter the escape state on the next char.
                #ifdef DEBUG
                printf("state state_general char_escape %i\n", selector);
                #endif
                next_state = STATE_IN_ESCAPE;
                break;
            }
            case (1000 * STATE_GENERAL + CHAR_WHITESPACE):{
                // This case may be extremely buggy dude to huge amount of code required....
                #ifdef DEBUG
                printf("state state_general char_whitespace %i\n", selector);
                #endif
                next_state = STATE_GENERAL;
                break;
            }
            case (1000 * STATE_GENERAL + CHAR_UNKNOWN):{
                // We will just treat unkowns as a CHAR_GENERAL at this point in time.
                #ifdef DEBUG
                printf("state state_general char_unknown %i\n", selector);
                #endif
                next_state = STATE_GENERAL;
                break;
            }
            case (1000 * STATE_GENERAL + CHAR_SPECIAL):{
                // Store as token.
                #ifdef DEBUG
                printf("state state_general char_special %i\n", selector);
                #endif
                if(c != '"' && !flag_literal) {
                    // If inside a string literal, teriminate buffer and add char as own token. 
                    if (buffpos > 0){
                        addtostring('\0', &buffpos, buffinc);
                        pushtoken(&nargs);
                        buffpos = 0;
                    }
                    addtostring(c, &buffpos, buffinc);
                    addtostring('\0', &buffpos, buffinc);
                    pushtoken(&nargs);
                    buffpos = 0;
                    *buffer = '\0';
                }

                if(c == '"'){ // Turn the string literal off every second occurance.
                   flag_literal = (flag_literal) ? 0: 1; 
                }

                #ifdef DEBUG
                printf("literal: %i\n", flag_literal);
                #endif

                next_state = STATE_GENERAL;
                break;
            }
            case (1000 * STATE_GENERAL + CHAR_END): 
                // Flush buffer to args?
                #ifdef DEBUG
                printf("state state_general char_end %i\n", selector);
                #endif
                if (buffpos > 0){
                    addtostring('\0', &buffpos, buffinc);
                    pushtoken(&nargs);
                }
                buffpos = 0;
                break;

       // STATE: IN_ESCAPE
            case (1000 * STATE_IN_ESCAPE + CHAR_GENERAL):{
                // Add char to buffer, let IN_WORD follow up.
                #ifdef DEBUG
                printf("state state_in_escape char_general %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_ESCAPE + CHAR_ESCAPE):{
                // Add char to buffer, let IN_WORD follow up.
                #ifdef DEBUG
                printf("state state_in_escape char_escape %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_ESCAPE + CHAR_UNKNOWN):{
                // Add char to buffer, let IN_WORD follow up.
                #ifdef DEBUG
                printf("state state_in_escape char_unknown %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_ESCAPE + CHAR_WHITESPACE):{
                // Add char to buffer, let IN_WORD follow up.
                #ifdef DEBUG
                printf("state state_in_escape char_whitespace %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_ESCAPE + CHAR_SPECIAL):{
                // Add char to buffer, let IN_WORD follow up.
                #ifdef DEBUG
                printf("state state_in_escape char_special %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_ESCAPE + CHAR_END):
                // We should actually escape this character and wait for more input.
                // For the mmoment, terminating buffer and flushing to args.
                #ifdef DEBUG
                printf("state state_in_escape char_end %i\n", selector);
                #endif
                addtostring('\0', &buffpos, buffinc);
                buffpos = 0;
                pushtoken(&nargs);
                break;

        // STATE: IN_WORD
            case (1000 * STATE_IN_WORD + CHAR_GENERAL):{
                // Store char in buffer. Continue on.
                #ifdef DEBUG
                printf("state state_in_word char_general %i\n", selector);
                #endif
                addtostring(c, &buffpos, buffinc);
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_WORD + CHAR_WHITESPACE):{
                // This is probably a word terminating case..., except if string literal is flagged.
                #ifdef DEBUG
                printf("state_in_word char_whitespace\n");
                #endif
                // Check String literal flag.
                // Terminate the buffer and push the token.
                if (!flag_literal){
                    addtostring('\0', &buffpos, buffinc);
                    buffpos = 0;
                    pushtoken(&nargs);
                    #ifdef DEBUG
                    printf("Buffer was... '%s'\n", buffer);
                    #endif
                    *buffer = '\0';
                    // Should we say next_state = STATE_IN_WORD; break; here?
                } else {
                    // Else we are taking this whitespace literally, so add to buffer.
                    addtostring(c, &buffpos, buffinc);
                }
                next_state = STATE_GENERAL;
                break;
            }
            case (1000 * STATE_IN_WORD + CHAR_SPECIAL):{
                // If string literal is flagged, add to token buffer... Just add to buffer I guess.
                #ifdef DEBUG
                printf("state_in_word char_special\n");
                #endif

                // Toggle string literal flag
                if (c == '"'){
                    flag_literal = (flag_literal == 1) ? 0: 1;
                }


                #ifdef DEBUG
                printf("literal: %i\n", flag_literal);
                #endif

                if (c != '"' && !flag_literal){
                    // Terminate the buffer, push buffer as token, push special char as token.
                    if(buffpos > 0){
                        addtostring('\0', &buffpos, buffinc);
                        pushtoken(&nargs);
                        buffpos = 0;
                    }
                    
                    addtostring(c, &buffpos, buffinc);
                    addtostring('\0', &buffpos, buffinc);
                    pushtoken(&nargs);
                    buffpos = 0;
 
                } else if (c != '"' && flag_literal){
                   // Just add to the buffer, because we want the literal character.
                   addtostring(c, &buffpos, buffinc);
                }

                next_state = STATE_GENERAL;
                break;
            }
            case (1000 * STATE_IN_WORD + CHAR_UNKNOWN):{
                // Just add it to the token buffer.
                #ifdef DEBUG
                printf("state_in_word char_unknown.\n");
                #endif
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_WORD + CHAR_ESCAPE):{
                // Flag the next char as being escaped.
                #ifdef DEBUG
                printf("state_in_word char_escape\n");
                #endif
                next_state = STATE_IN_WORD;
                break;
            }
            case (1000 * STATE_IN_WORD + CHAR_END):
                // Terminate buffer, flush to args.
                #ifdef DEBUG
                printf("state_in_word char_end\n");
                #endif
                addtostring('\0', &buffpos, buffinc);
                buffpos = 0;
                pushtoken(&nargs);
                #ifdef DEBUG
                printf("Buffer was... '%s'\n", buffer);
                #endif
                break;

       // STATE: FALL THROUGH!
            default: {
                fprintf(stderr, "Case not caught! State Machine is broken and the programmer is a scrub. (STATE: %i, CHAR: %c)\n", (int) current_state, c );
            }
        }
        // Advance
        c = *line++; 
        current_state = next_state;
    } while (cat != CHAR_END); 

    #ifdef DEBUG
    printf("LAST BUFFER: %s\n", buffer);
    #endif

    if(buffer != NULL){
       free(buffer);
    }

    return nargs;
}

void addtostring(char c, int *buffpos, int inc){
    // Add character to the string at `buffpos`, resize (by `inc`) when needed.
    if (*buffpos % inc == 0 && *buffpos > 0){
        char *t = realloc(buffer, (*buffpos + inc) * sizeof(char));
        if (t == NULL){
            fprintf(stderr, "realloc in addtostring() failed.\n");
            fflush(stderr);
            abort();
        } else {
            buffer = t;
        }
    }
    *(buffer + (*buffpos)++) = c;
    return;
}

void pushtoken(int *size){
     // Add a token to the args string array. Looks for a terimating NULL pointer overwrites it 
     // with the new token and adds a new terminating NULL pointer.

     if (*size > 0){
         args = realloc(args, (*size + 2) * sizeof(char *));
     } else {
         args = malloc(2 * sizeof(char *));
     }

     if (args == NULL){
         fprintf(stderr, "alloc of args array failed.\n");
         fflush(stderr);
         abort();
     }
     
     args[*size] = buffer; // Add token, overwrite old NULL...
     args[*size + 1] = (char*) NULL; // Add the new terminator.
     *size += 1;

     #ifdef DEBUG
     printf("Pushed token: '%s'\n", buffer);
     #endif

     char *tk = malloc(sizeof(char)); // Set up some new memory for the next token, else we'll overwrite!.
     if (tk == NULL){
        fprintf(stderr, "malloc in pushtoken() failed.\n");
        fflush(stderr);
        abort();
     } else {
         *tk = '\0'; // Initialise as empty...
         buffer = tk;
     } 

    return;
}

int cd(){
   // Basic implementation of the Change Directory `cd` command. Return negative on failure.

   if (args[1] != (char *) NULL){
       if (chdir(args[1]) == -1){
           perror("cd"); 
       }
   } else {
       fprintf(stderr, "You must supply a valid argument to the 'cd' command.");
       return EXIT_FAILURE;
   }
   return EXIT_SUCCESS;
}

void child_handler(int sig){
    // A signal handler to handle child termination.

   int status;
   pid_t pid;
   while(1){
       pid = waitpid(-1, &status, WNOHANG);
       int pos = -1;
       for (int i = 0; i < 100; i++){
           if (pidarr[i] == pid){
               pos = i + 1;
               break;
           } 
       }

       if(pos > -1){
           printf("[%i] %i Done!\n", pos, (int) pid);
       } else {
           printf("[>100] %i Done!\n", (int) pid);
       }
       processes--;
       fflush(stdout);
       break;
   }
   return;
}