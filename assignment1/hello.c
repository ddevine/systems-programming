



/*
* Author: Daniel Devine
* Date: 27/02/2013
* Description: A simple hello world.
*/

	


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
    printf("Hello World\n");
    return EXIT_SUCCESS;
}
