#ifndef SEMA_H
    #define SEMA_H
    // The _GNU_SOURCE macro must go before stdlib
    #define _GNU_SOURCE

    #include <stdio.h>
    #include <stdlib.h>
    #include <pthread.h>

    typedef struct semaphore {
        pthread_mutex_t lock;
        int value;
        pthread_cond_t cond;
    } semaphore;

    void procure(semaphore *s);
    void vacate(semaphore *s);

    void init(semaphore *sema, int initial_value);
    void destroy(semaphore *s);
#endif
