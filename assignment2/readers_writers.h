#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <dispatch/dispatch.h>
#include "sema.h"

void get_random_uint16(FILE *source, uint16_t *dest);

// An Event Counter type.
typedef semaphore event_counter; // An event counter is simply a counting semaphore.

typedef struct Sequencer {  // Not just using a semaphore because I want to keep locking separate from value.
    semaphore sema; // Value must be incremented atomically.
    int value;
} sequencer;


typedef struct buffer {
    uint16_t *buffer;
    int max_size;
    int min_fill_level;
    int size;
    uint16_t *head; // The least recent entry in queue.
    uint16_t *tail; // The most recent entry in queue. The tail grows. 
    event_counter ec_put;
    sequencer seq_put;
    event_counter ec_take;
    sequencer seq_take;
} buffer;

void init_buffer(buffer *buff, int max_size, int min_fill_level); // Note buff is the pointer to the circular buffer to initialise.
void destroy_buffer(buffer *buff); // Frees memory allocated to buffer.


void put_buffer(buffer *buff, uint16_t *value); // Put stuff in the buffer
uint16_t get_buffer(buffer *buff); // Returns the last value put in.


void init_sequencer(sequencer *seq);
void destroy_sequencer(sequencer *seq);

int ticket(sequencer *seq);

void init_ec(event_counter *ec);
void destroy_ec(event_counter *ec);
int read(event_counter *ec);
void advance(event_counter *ec);
void await(event_counter *ec, int value); // We will diddle with the semaphore's internals for this one.
