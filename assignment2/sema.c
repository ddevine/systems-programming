/*
** Author: Daniel Devine
** Date: 24/04/2013
** Description: Exercise with semaphores.
*/

#include "sema.h"

void init(semaphore *sema, int initial_value){
    // Sets up a semaphore, initialises value
    pthread_mutex_init(&sema->lock, NULL);
    sema->value = initial_value; // A semaphore should be initially claimable... Else it would have to be vacated (++) before usage.
    pthread_cond_init(&sema->cond, NULL);
}

void destroy(semaphore *sema){
    // Cleans up properly after the use of a Semaphore (e.g. destroys all mutexes, and condition variables, and releases all memory allocated for a Semaphore).
    pthread_mutex_destroy(&sema->lock);
    pthread_cond_destroy(&sema->cond);
}

void procure(semaphore *sema){
    // Waits for a lock, waits to claim semaphore. Decrements value.
    // If the value is equal to or less than 0, it will wait until another thread increments to 1.

    //begin_critical_section(semaphore);  // make the following concurrency-safe - lock
    pthread_mutex_lock(&sema->lock);
    //while (semaphore->value <= 0)
    while(sema->value <= 0){
	//wait_for_vacate(semaphore);     // wait for signal from vacate()
        pthread_cond_wait(&sema->cond, &sema->lock);
    }
    //semaphore->value--;                 // claim the Semaphore
    sema->value--;
    //printf("procure %i\n", sema->value);
    //end_critical_section(semaphore);    // unlock
    pthread_mutex_unlock(&sema->lock);
}

void vacate(semaphore *sema){
    // Releases a semaphore, signals availability. Increments value.

    //begin_critical_section(semaphore);  // make the following concurrency-safe, lock
    pthread_mutex_lock(&sema->lock);
    //semaphore->value++;                 // release the Semaphore
    sema->value++;
    //printf("vacate %i\n", sema->value);
    //signal_vacate(semaphore);           // signal anyone waiting on this
    pthread_cond_signal(&sema->cond);
    //end_critical_section(semaphore);    // unlock
    pthread_mutex_unlock(&sema->lock);
}


// GCD Notes
// Blocks are basically anonymous functions. Defined with nonstandard ^{  code  }
// Blocks are put into queues.
// Jobs/Queues are enforced with x_sync()
// Jobs can be added to queues asynchronously.
// Groups can be used to manage job queues - allows you to not have to enforce a managed heirarchy. Can use a nice wait() to enforce syncronisation.
// the _f() versions of functions are ones which you can pass function pointers to instead of using blocks.
// Blocks have the advantage of being closures, which means you don't have to pass a specific context (variables for the function to use).
// TODO: learn what the const keyword does exactly.
