/*
* Author: Daniel Devine
* Date: 18/04/2013
* Description: A multithreaded Hello World.
*/

#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *childhello();

int main(int argc, char **argv){
    // A "Hello World" program hello.c that creates a child thread which prints "Hello Child". 
    // After creating the child thread, the parent thread needs to print "Hello Parent", then wait for the child thread to finish before itself exiting."
  
    pthread_t childthread; // This is effectively our handle on the thread.

    int rc = pthread_create(&childthread, NULL, childhello, (void *) NULL);

    if (rc){ // If non-negative
        fprintf(stderr, "pthread_create() returned an error: %d\n", rc);
        return EXIT_FAILURE;
    }

    // Wait for child thread to complete...
    rc = pthread_join(childthread, NULL);
    if (rc){
        fprintf(stderr, "pthread_join() returned an error: %d\n", rc);
        return EXIT_FAILURE;
    }
    
    printf("Hello Parent\n");

    return EXIT_SUCCESS;
}

void *childhello(){
    printf("Child Hello\n");
    return NULL;
}
