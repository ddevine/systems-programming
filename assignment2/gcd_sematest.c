/*
* Author: Daniel Devine
* Date: 18/04/2013
* Description: An exercise in syncronisation. Text entered in main thread, child prints after text is entered, exits when return is entered. This example uses Grand Central Dispatch API.
*/

#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <dispatch/dispatch.h>

int main(int argc, char **argv){
    // Set up parent_queue
    dispatch_queue_t parentQueue;

    // Set up child_queue
    dispatch_queue_t childQueue;

    // Get the global dispatch queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    parentQueue = dispatch_queue_create("parent", NULL);
    childQueue = dispatch_queue_create("child", NULL);

    dispatch_group_t group = dispatch_group_create();

    dispatch_group_async(group, queue, ^{
        // Within this group, we can dispatch tasks onto our child and parent queues.

        dispatch_sync(parentQueue,  // 
        ^{
            char *inputstr = NULL; // This will be in scope for child blocks...
            size_t len = 0;

            printf("Enter some text\n");
            if (getline(&inputstr, &len, stdin) == -1){
                perror("getline()");
            }

            dispatch_sync(childQueue, // Do the printing
            ^{
                printf("%s", inputstr);

                if (inputstr != NULL){
                   free(inputstr);
                }
            });

        });

        dispatch_sync(parentQueue,
        ^{ // We must do this on the parent queue to replicate the original program.
            printf("Press Enter...\n");
            while (getc(stdin) != '\n');            
        });
    });

    // Wait for all tasks in the group to be completed.
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);

    // Clean up the group
    dispatch_release(group);

    return EXIT_SUCCESS;
}
