/*
Solve the reader/writer problem by implementing a network status monitor that tracks network statistics on the system. The writer should monitor packet numbers once per second and update a status area containing the number of correctly transmitted and erroneous input and output packets as well as a time stamp of the update. The reader(s) should be able to read and display the latest status information at any time. A writer trying to update the status information needs to have priority over readers trying to read the information.
*/

#include "NetStat/netstat.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
    
    return EXIT_SUCCESS;
}
