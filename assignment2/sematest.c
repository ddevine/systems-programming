/*
* Author: Daniel Devine
* Date: 18/04/2013
* Description: An exercise in syncronisation. Text entered in main thread, child prints after text is entered, exits when return is entered.
*/

// This is an exercise of binary semaphores - not counting semaphores (for Readers-Writers/circular buffers).

#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "sema.h"

#include <unistd.h> // For sleep

void *printstr();

typedef struct {
    char *inputstr;
    semaphore printer; // For signalling printing start/stop.
    semaphore reader;
    int valid;
    int exit; // A flag to signal if the printer should exit.
} targs; 

int main(int argc, char **argv){
    pthread_t childthread; // This is effectively our handle on the thread.

    size_t len = 0;

    targs child_args;

    // Initialise printer semaphores
    init(&child_args.printer, 0);
    init(&child_args.reader, 0);

    child_args.exit = 0; // We don't want the child to immediately terminate.

    int rc = pthread_create(&childthread, NULL, printstr, (void *) &child_args); // Create thread
    if (rc){ // If non-negative
        fprintf(stderr, "pthread_create() returned an error: %d\n", rc);
        return EXIT_FAILURE;
    }

    printf("Enter some text\n");
    if (getline(&child_args.inputstr, &len, stdin) == -1){
         perror("getline()");
    }

    child_args.valid = 1;

    vacate(&child_args.printer); // Signal the child (printer) that it can print.
    procure(&child_args.reader); // Wait for signal that child has printed.

    printf("Press Enter...\n");
    while (getc(stdin) != '\n');

    child_args.exit = 1;

    vacate(&child_args.printer); // Signal printer to unblock. It will see exit == 1, vacate and exit.

    // Wait for child to finish. Should see exit == 1 now it is unblocked.
    while(pthread_join(childthread, NULL) != 0); // Wait for the thread to exit before continuing.

    // Cleanup semaphores
    destroy(&child_args.reader);
    destroy(&child_args.printer);

    if (child_args.inputstr != NULL){
        free(child_args.inputstr);
    }

    return EXIT_SUCCESS;
}

void *printstr(targs *t){
    // A thread task for printing a string. 

    // What happens if we add a sleep here?
    printf("Child thread sleeping for 5 seconds...\n");
    sleep(5); // Nothing breaks!
 
    while(1){
        procure(&t->printer); // Wait to be signalled to print.
        //printf("procuring printer\n");

        if(t->exit == 1){ // Exit if we're told to. 
            vacate(&t->printer); // Release lock on the printer.
            break;
        }

        if (t->inputstr != NULL){
            fputs(t->inputstr, stdout);
        }
        vacate(&t->reader); // Signal reader that we have printed.
        //printf("vacate reader\n");
    }
    pthread_exit(NULL); // Or just return NULL;
}
