#include "readers_writers.h"


// Global buffer to allow modification within closures.
buffer buff;

int main(int argc, char **argv){
    // Create a test program that uses dispatch_async() to spawn the concurrent producer. I.e. the task should use the function you created earlier to read values from /dev/random as fast as it can and immediately put each value into the buffer. A separate task should act as the consumer: it should read and remove one number from the buffer, and print it to stdout as a 4 digit lower-case hex numbers prefixed by 0x (e.g. 0xfeed for the decimal number 65261).
    
    int stopFlag = 0;

    // Set up queues
    dispatch_queue_t producerQueue;
    dispatch_queue_t consumerQueue;

    // Get the global dispatch queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    consumerQueue = dispatch_queue_create("consumer", NULL);
    producerQueue = dispatch_queue_create("producer", NULL);
    
    dispatch_group_t group = dispatch_group_create();

    // If we set up the buffer here then it would be made read-only by the blocks (closures).

    dispatch_group_async(group, queue, ^{
        // Within this group, we can dispatch tasks onto our child and parent queues.
        dispatch_async(producerQueue, ^{
            // Produce here
            // Use a while !stop here to keep stuffing values into the buffer.
            
            printf("Launched producer\n");
            
            init_buffer(&buff, 2, 1); // Max_size = 1, min_fill_level = 1;
            
            
            FILE *source = NULL;
           
            source = fopen("/dev/random", "r");
            
            uint16_t dest;
               
            while(!stopFlag){
                get_random_uint16(source, &dest);
                put_buffer(&buff, &dest);
            }
            
            printf("Stopping producer\n");
            
            // Clean up buffer.
            destroy_buffer(&buff);
        });
        
        dispatch_async(consumerQueue, ^{
            printf("Launched consumer\n");
            while(!stopFlag){
                printf("Consumed: %i\n", (int) get_buffer(&buff));
            }
            printf("Stopping consumer\n");
        });
        
    });
    
    
    // Block main thread here, wait for enter to be pressed before setting stop flag. 
    while (getc(stdin) != '\n');
    printf("STOP!\n");
    stopFlag = 1;

    // Wait for all tasks in the group to be completed.
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);

    // Clean up the group
    dispatch_release(group);

    return EXIT_SUCCESS;
}

void get_random_uint16(FILE *source, uint16_t *dest){
    // Reads high quality 16 bit random numbers of type uint16_t (as defined in <stdint.h>) from /dev/random as fast as it can.
    // To make it fast we hand in an open file pointer, and a pointer for the result (so there is no copying).
    static const size_t size = sizeof(uint16_t);
    fread(dest, size, 1, source); // Remember, fread returns number of successful 'blocks'.
}

void init_buffer(buffer *buff, int max_size, int min_fill_level){
    // Initialises the buffer.
    buff->buffer = malloc(sizeof(uint16_t) * max_size);
    
    buff->min_fill_level = min_fill_level;
    buff->size = 0;
    buff->max_size = max_size;
    buff->head = buff->buffer;
    buff->tail = buff->buffer;
    
    init_ec(&buff->ec_put);
    init_sequencer(&buff->seq_put);
    
    init_ec(&buff->ec_take);
    init_sequencer(&buff->seq_take);
}


void destroy_buffer(buffer *buff){
   // Frees buffer allocated memory.

   destroy_sequencer(&buff->seq_put);
   destroy_sequencer(&buff->seq_take);
   
   destroy_ec(&buff->ec_take);
   destroy_ec(&buff->ec_put);

   free(&buff->buffer);
}

void put_buffer(buffer *buff, uint16_t *value){
    // Add something to the buffer.

    // Add a construct to your put_buffer() function that waits for space to become available when the buffer is full.

    printf("PUT Size: %i Max: %i\n", buff->size, buff->max_size);

    if (buff->size >= buff->max_size){ // Block until there is space in the buffer.
        printf("Wait to fill\n");
        await(&buff->ec_take, ticket(&buff->seq_take)); // Wait for next Take event.
    }

    *(buff->tail++) = *value;
    buff->size++;
    advance(&buff->ec_put); // Let threads waiting for data know there is some. Creates a "Put" event.
}

uint16_t get_buffer(buffer *buff){
    // Function that reads the oldest value that was put into the buffer, removes that value from the buffer and returns it. For the moment, you can ignore the minimum fill level and concurrency.

    //  Also add a construct to your get_buffer() function that, if the minimum fill level would be compromised, waits for more data to arrive in the buffer before removing an integer from the buffer.

    printf("GET Size: %i Min: %i\n", buff->size, buff->min_fill_level);

    if ((buff->size - 1) <= buff->min_fill_level){ // Block if the minimum_fill_level is going to be broken.
        printf("Wait to take\n");
        await(&buff->ec_put, ticket(&buff->seq_put));
    }
    
    buff->size--;
    int r = *buff->head;
    buff->head++;
    advance(&buff->ec_take); // Let threads waiting know there is now space in the buffer.
    return r;
}

// Sequencers are closely linked with event counters. A Sequencer is also an integer (initialised to 0) that is used to serialise the occurrence of events. A sequencer only has one atomic operation, ticket(), that returns the current value of the sequencer and then increments the sequencer by one.

void init_sequencer(sequencer *seq){
    seq->value = 0;
    init(&seq->sema, 1); // Not sure about this init value...
}

void destroy_sequencer(sequencer *seq){
    destroy(&seq->sema);
}

int ticket(sequencer *seq){
   // Atomically returns current value of sequencer seq, and increments sequencer value by 1. 
   int16_t value;
   procure(&seq->sema);
   value = seq->value++;
   vacate(&seq->sema);
   return value;
}

/*
An Event Counter is another task synchronisation instrument that consists of an integer (with an initial value of 0) and atomic operations. The atomic operations are as follows:

read(Eventcounter)
    returns the current (integer) value of Eventcounter
advance(Eventcounter)
    increments the Eventcounter by one
await(Eventcounter, value)
    blocks the current thread until Eventcounter >= value
*/

void init_ec(event_counter *ec){
    // Initialises an event counter.
    ec = malloc(sizeof(*ec)); // Size of ec itself, not the size of the pointer.
    init(ec, 0);
}

void destroy_ec(event_counter *ec){
   // Destroys event counter, de-allocates memory.
   destroy((semaphore *) ec);
}

int read(event_counter *ec){
   // Returns the current integer value of the event_counter ec.
   // Atomicity concerns here?
   return ec->value;
}

void advance(event_counter *ec){
    // Atomically increments event counter by 1.

    // Rely on the semaphore's atomicity?
    vacate((semaphore*) ec);
}

void await(event_counter *ec, int value){
    // Blocks current thread until Eventcounter >= value.

    // Repurposing the guts of the semaphore procure() to act as a wait for value for a counting semaphore.
    pthread_mutex_lock(&ec->lock);
    if (value >= 0){
        while(ec->value <= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    } else { // But we should handle negatives...
        while(ec->value >= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    }

    pthread_mutex_unlock(&ec->lock);
}

