/*
* Author: Daniel Devine
* Date: 18/04/2013
* Description: An exercise in syncronisation. Text entered in main thread, child prints after text is entered, exits when return is entered.
*/

#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *printstr();

typedef struct {
    char *inputstr;
    pthread_mutex_t lock;
    pthread_mutex_t output_lock; // Unlocked after output processed.
    int exit;
} targs; 

int main(int argc, char **argv){
    pthread_t childthread; // This is effectively our handle on the thread.

    size_t len = 0;

    targs child_args;
    pthread_mutex_init(&child_args.lock, NULL); // Set up locking mutex
    pthread_mutex_init(&child_args.output_lock, NULL); // Set up printing lock

    pthread_mutex_lock(&child_args.lock); // Lock, don't allow ouput before buffer filled.
    pthread_mutex_lock(&child_args.output_lock); // Lock until priting is done

    child_args.exit = 0; // We don't want the child to immediately terminate.

    int rc = pthread_create(&childthread, NULL, printstr, (void *) &child_args); // Create thread
    if (rc){ // If non-negative
        fprintf(stderr, "pthread_create() returned an error: %d\n", rc);
        return EXIT_FAILURE;
    }

    printf("Enter some text\n");
    if (getline(&child_args.inputstr, &len, stdin) == -1){
         perror("getline()");
    }

    pthread_mutex_unlock(&child_args.lock); // Let the child thread print

    while(pthread_mutex_trylock(&child_args.output_lock) != 0); // Wait for child to print
    
    printf("Press Enter...\n");
    while (getc(stdin) != '\n');

    child_args.exit = 1; // Make the child return.

    while(pthread_join(childthread, NULL) != 0); // Wait for the thread to exit before continuing.

    // Cleanup
    pthread_mutex_destroy(&child_args.lock);
    pthread_mutex_destroy(&child_args.output_lock);

    if (child_args.inputstr != NULL){
        free(child_args.inputstr);
    }

    return EXIT_SUCCESS;
}

void *printstr(targs *t){
    // A thread task for printing a string.
    while(t->exit == 0){
        if(pthread_mutex_trylock(&t->lock) == 0){
            if (t->inputstr != NULL){
                fputs(t->inputstr, stdout);
            }
            pthread_mutex_unlock(&t->output_lock);
        }
    }
    pthread_exit(NULL); // Or just return NULL;
}
